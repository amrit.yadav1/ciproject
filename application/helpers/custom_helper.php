<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('new_method'))
{
    function new_method($var,$message,$type)
    {
        $array["type"] =$type;
        $array["message"] =$message;
        $array["value"] =$var;
        // print_r($array);
        $var = json_encode($array);
         return $var;
    }   

    function pre($data){
        echo '<pre>';
        print_r($data);
        exit;
    }
    function generateRandomString($length = 30) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}