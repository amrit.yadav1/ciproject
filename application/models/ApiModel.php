<?php
class ApiModel extends CI_Model
{
 public function getApiData()
 {
     $this->db->select('id,name,email');
     $this->db->where('type', 'admin');
    
     $UserData = $this->db->get('employee')->result_array();

    //   echo $this->db->last_query();exit;
     return $UserData;
 }
 public function checktoken($data)
    {
        
        $this->db->select('token_key');
        $this->db->where('token_key',$data['token_key']);
        $tokenval=$this->db->get('api_token')->row_array();
        //       echo $this->db->last_query();
        //  exit();
        return $tokenval;
    }
    public function insertApiData($data)
    {
       

        $arrayresult=array();
        $dataval = json_decode($data, true);
        
        foreach($dataval["value"] as $rowdataval )
        {
            unset($rowdataval["id"]); 
            $arrayresult[]=$rowdataval;
            //use when add new element in array list
            // $arrayresult[]=
            // ['name'=>$rowdataval['name'],'email'=>$rowdataval['email'],'date'=>"value1"];
          
         
        }
       

        //  pre($arrayresult); exit();
         $this->db->insert_batch('emp_invokedata', $arrayresult);

         $insert_id = $this->db->insert_id();
         return $insert_id;
     

    }
}