<?php
defined('BASEPATH') or exit('No direct script access allowed');

class HomeModel extends CI_Model
{
    public function submitdetails($data)
    {
        // print_r($data);
        // exit();     
        $mydata = [
            "name" => isset($data['name']) ? $data['name'] : ' ',
            "password" => isset($data['password']) ? md5($data['password']) : '',
            "email" => isset($data['email']) ? $data['email'] : '',
            "Address" => isset($data['Address']) ? $data['Address'] : '',
            "type" => 'Admin'

        ];

        $this->db->insert('employee', $mydata);

        $insert_id = $this->db->insert_id();

        if ($insert_id > 0) {
            return  $insert_id;
        } else {
            return  false;
        }
    }
 
    public function logindetails($data)
    {

        $this->db->select('*');
        $this->db->where('name', $data['name']);
        $this->db->where('password', md5($data['password']));
        $this->db->where('type', 'admin');


        $query =  $this->db->get('employee')->row_array();
       
        // echo generateRandomString();
        // return $query;

        if (!empty($query)) {

            // generateRandomString();
            $tokenvalue = [
                'userid' => $query['id'],
                'token_key' => generateRandomString(50)

            ];
            $tval = $this->db->insert('api_token', $tokenvalue);
            if ($tval) {
                $query['token_key']=$tokenvalue['token_key'];
                $this->session->set_userdata($query);
                return  $tokenvalue;
            } else {

                return [];
            }
        } else {
            return [];
        }
    }
}
