<?PHP
class MasterModel extends CI_Model
{
    
    public function getUserData()
    {
        $this->db->select('id,name,email');
        $this->db->where('type', 'Admin');
        $this->db->where('id',$_SESSION['id']);
        $UserData = $this->db->get('employee')->result_array();

        //  echo $this->db->last_query();exit;
        return $UserData;
    }

    public function checkacesstoken($data)
    {
        $this->db->select('token_key');
        $this->db->where('token_key',$data['access_token']);
        $tokenval=$this->db->get('api_token')->row_array();
        //       echo $this->db->last_query();
        //  exit();
        return $tokenval;
    }
    public function updateData($data)
    {
        // print_r($data['id']);
        // exit();
        $updatedata = [
            "name" => $data['name'],
            "email" => $data['email']
        ];
        $this->db->where('id', $data['id']);
        $updatedata = $this->db->update('employee', $updatedata);
        return $updatedata;
        //     echo $this->db->last_query();
        //  exit();

    }
    // public function getAllEmployeeExcel($reqParameter) 
    // {
    //     print_r($reqParameter);exit();

        
    // }

}
