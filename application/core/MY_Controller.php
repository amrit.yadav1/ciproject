<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class MY_Controller extends CI_Controller
{
    public $viewBag;

    public function __construct()
    {
        parent::__construct();
        
        if (!$this->session->userdata('id')) {
           
            redirect(base_url()."login", 'refresh');
            
        } 
        

    }
}
