<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>REC | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/bootstrap/css/bootstrap.min.css">
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/angular-block-ui/0.2.2/angular-block-ui.min.css"/> -->
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/dist/css/skins/_all-skins.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/plugins/iCheck/flat/blue.css">
<!-- Morris chart -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/plugins/morris/morris.css">
<!-- jvectormap -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Date Picker -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/plugins/datepicker/datepicker3.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/plugins/daterangepicker/daterangepicker-bs3.css">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!--Angular datepicker -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/dist/angular-datepicker.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/angucomplete-alt/angucomplete-alt.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/angularjs-toastr/angular-toastr.min.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/autocomplete/autocomplete_css/angucomplete-alt.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/autocomplete/autocomplete_css/fonts/bariol/bariol.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/autocomplete/autocomplete_css/angucomplete.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/angular-ui-select/dist/select.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/datatables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/blockUI/angular-block-ui.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/angular-confirm/dist/angular-confirm.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/sweet/sweetalert.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/sweet/sweetalert.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nvd3/1.8.1/nv.d3.min.css"/>
<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
    .pills-struct .tab-pane {
        padding-top: 15px; }

    .nav-pills > li {
        margin-right: 5px; }
    .nav-pills > li > a {
        border: none;
        padding: 10px 20px;
        color: #878787;
        margin: 0;
        border-radius: 0;
        text-transform: capitalize; }

    .nav-pills-rounded.nav-pills > li > a {
        border-radius: 60px; }
    
    .navbar-nav>li>a {
    line-height: 2px;
}
.add-employee .xcrud-view .form-horizontal .form-group {
        width: 50% !important;
        float: left !important;
    }
.content-header > .breadcrumb
{
float: right;
background: transparent;
margin-top: 0;
margin-bottom: 0;
font-size: 12px;
padding: 7px 5px;
position: absolute;
top: 15px;
right: 10px;
border-radius: 2px;
}

</style>
<script>
    var base_url = "<?php echo base_url(); ?>"
</script>