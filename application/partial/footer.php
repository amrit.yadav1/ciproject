</body>
<style>






    .overlay {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.7);
        transition: opacity 500ms;
        visibility: hidden;
        opacity: 0;
        z-index: 20px;
    }
    .overlay:target {
        visibility: visible;
        opacity: 1;
    }

    .popup {
        margin: 270px auto;
        padding: 20px;
        background: #fff;
        border-radius: 5px;
        width: 30%;
        position: relative;

    }

    .popup h2 {
        margin-top: 0;
        color: #333;
        font-family: Tahoma, Arial, sans-serif;
    }
    .popup .close {
        position: absolute;
        top: 2px;
        right: 8px;
        transition: all 200ms;
        font-size: 30px;
        font-weight: bold;
        text-decoration: none;
        color: #333;
    }
    .popup .close:hover {
        color: #06D85F;
    }
    .popup .content {
        max-height: 30%;
        overflow: auto;
    }

    @media screen and (max-width: 700px){
        .box{
            width: 70%;
        }
        .popup{
            width: 70%;
        }
    }
</style><style scoped>

    .button-success,
    .button-error,
    .button-warning,
    .button-secondary {
        color: white;

        text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
    }

    .button-success {
        float:right;
        background: rgb(28, 184, 65); /* this is a green */
    }

    .button-error {  float:right;
                     background: rgb(202, 60, 60); /* this is a maroon */
    }

    #cover-spin {
        position:fixed;
        width:100%;
        left:0;right:0;top:0;bottom:0;
        background-color: rgba(255,255,255,0.7);
        z-index:9999;
        display:none;
    }

    @-webkit-keyframes spin {
        from {-webkit-transform:rotate(0deg);}
        to {-webkit-transform:rotate(360deg);}
    }

    @keyframes spin {
        from {transform:rotate(0deg);}
        to {transform:rotate(360deg);}
    }

    #cover-spin::after {
        content:'';
        display:block;
        position:absolute;
        left:48%;top:40%;
        width:40px;height:40px;
        border-style:solid;
        border-color:#42a5f5;
        border-top-color:transparent;
        border-width: 4px;
        border-radius:50%;
        -webkit-animation: spin .8s linear infinite;
        animation: spin .8s linear infinite;
    }

</style> 
<button style="display:none" id="myid" onclick=" $('#cover-spin').show(0)">Save</button>

<button style="display:none" id="myid2" onclick="$('#cover-spin').hide(0)">Save</button>
<script>

    function startloder() {

        document.getElementById('myid').click();
    }
    function stoploder() {
        document.getElementById('myid2').click();
    }
    document.getElementById('closeLoaderId').click();
    document.getElementById('closeAlertId').click();
    function popupN(msg) {
        showalertFunction('', msg);
    }
    function popup(msg) {
        showalertFunction('warning', msg);
    }
    function popupS(msg) {
        showalertFunction('success', msg);
    }
    function  showalertFunction(messageType, message) {
        swal(message, "", messageType);
    }
</script>
<div id="cover-spin"></div>

<div id="loader" class="overlay" >
    <div class="popup" style="background-color: transparent;">
        <!--        <h4 align="center"><b>Warning</b></h4> -->
        <a style="display:none" class="close" id="closeLoaderId" href="#">&times;</a>
        <p align="center"> <img id='loderImg' width='100'  height="100" src="<?php echo base_url(); ?>assets/img/ajax_loader.gif"></p>

    </div>
</div>
<div id="alert" class="overlay">
    <div class="popup">
        <!--        <h4 align="center"><b>Warning</b></h4> -->
        <a class="close" id="closeAlertId" href="#">&times;</a>
        <br/>   
        <h4 align="center" class="content"  >
            <b id="errorContent">
            </b>
        </h4>
    </div>
</div>

<div class="box">
    <a class="button" href="#alert" id="showLAert" style="display:none">Alert</a>
    <a class="button" href="#confirmbox" id="showLConfirm" style="display:none">Confirm</a>
    <a class="button" href="#loader" id="showLLoader" style="display:none" >Loader</a>
</div>
<!--JAVASCRIPT-->
<!--=================================================-->

<!--Pace - Page Load Progress Par [OPTIONAL]-->
<link href="<?php echo base_url(); ?>assets/plugins/pace/pace.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/plugins/pace/pace.min.js"></script>


<!--jQuery [ REQUIRED ]-->



<!--BootstrapJS [ RECOMMENDED ]-->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>


<!--NiftyJS [ RECOMMENDED ]-->
<script src="<?php echo base_url(); ?>assets/js/nifty.min.js"></script>
<!--X-editable [ OPTIONAL ]-->
<script src="<?php echo base_url(); ?>assets/plugins/x-editable/js/bootstrap-editable.min.js"></script>



<!--Chosen [ OPTIONAL ]-->
<script src="<?php echo base_url(); ?>assets/plugins/chosen/chosen.jquery.min.js"></script>

<!--Form Component [ SAMPLE ]-->
<script src="<?php echo base_url(); ?>assets/js/demo/form-component.js"></script>


<!--Dropzone [ OPTIONAL ]-->
<script src="<?php echo base_url(); ?>assets/plugins/dropzone/dropzone.min.js"></script>


<!--Form File Upload [ SAMPLE ]-->
<!--<script src="<?php // echo base_url();    ?>assets/js/demo/form-file-upload.js"></script>-->






<!--=================================================-->

<!--Background Image [ DEMONSTRATION ]-->
<script src="<?php echo base_url(); ?>assets/js/demo/bg-images.js"></script>




<!-- Mirrored from www.themeon.net/nifty/v2.6/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 29 Apr 2017 07:35:21 GMT -->
</html>
