    <?php $totalCount = $this->viewBag['notification']["RMleave_req"] + $this->viewBag['notification']["RM_ar_req"] + $this->viewBag['notification']["RM_od_req"]+$this->viewBag['notification']["myRelocationReq"]+$this->viewBag['notification']["rmRelocation"]+$this->viewBag['notification']["existRm"] + $this->viewBag['notification']["ProjectEndCount"]; ?>
<li class="dropdown notifications-menu" style="display:<?php echo ($this->viewBag['notification']["isRM"]||$this->viewBag['notification']["myRelocationReq"]||$this->viewBag['notification']["rmRelocation"]) ? "" : "none" ?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <i class="fa fa-bell-o"></i>
        <span style="display:<?php echo ($totalCount > 0) ? "" : "none"?>"class="label label-danger"> <?php echo $totalCount; ?> </span>
    </a>

    <ul class="dropdown-menu">
        <li class="header">You have <?php echo $totalCount; ?> Pending Requests</li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <li>
                    <a href="<?php echo base_url() ?>leaveapproval" style="display:<?php echo ($this->viewBag['notification']["RMleave_req"]) ? "" : "none"; ?>" id="updatenotification" notificationtype="leaverequest">
                        <i class="fa fa-users text-red"></i> <?php echo $this->viewBag['notification']["RMleave_req"] ?> Leave Requests Pending to Approve
                    </a>
                </li>
                <li>
                    <a style="display:<?php echo ($this->viewBag['notification']["RM_ar_req"]) ? "" : "none"; ?>"href="<?php echo base_url() ?>teamAttendance/4" id="updatenotification" notificationtype="arrequest">
                        <i class="fa fa-users text-green"></i> <?php echo $this->viewBag['notification']["RM_ar_req"] ?> Attendance Regularization Requests Pending to Approve
                        page and may cause design problems
                    </a>
                </li>
                <li>
                    <a  href="<?php echo base_url() ?>teamAttendance/5" style="display:<?php echo ($this->viewBag['notification']["RM_od_req"]) ? "" : "none"; ?>"id="updatenotification" notificationtype="odrequest">
                        <i class="fa fa-users text-aqua"></i>  <?php echo $this->viewBag['notification']["RM_od_req"] ?> On Duty Requests Pending to Approve
                    </a>
                </li>
                <li>
                    <a  href="<?php echo base_url() ?>teamAttendance/5" style="display:<?php echo ($this->viewBag['notification']["approved_od"]) ? "" : "none"; ?>"id="updatenotification" notificationtype="apprequest">
                        <i class="fa fa-users text-aqua"></i>  <?php echo $this->viewBag['notification']["approved_od"] ?> Approved On Duty Requests
                    </a>
                </li>
                <li>
                    <a  href="<?php echo base_url() ?>emprealocation" style="display:<?php echo ($this->viewBag['notification']["myRelocationReq"]) ? "" : "none"; ?>"id="updatenotification" notificationtype="myrelocationrequest">
                        <i class="fa fa-users text-yellow"></i>  <?php echo $this->viewBag['notification']["myRelocationReq"] ?> My Relocation Request
                    </a>
                </li>
                <li>
                    <a  href="<?php echo base_url() ?>rmrealocation" style="display:<?php echo ($this->viewBag['notification']["rmRelocation"]) ? "" : "none"; ?>"id="updatenotification"notificationtype="relocationrequest">
                        <i class="fa fa-users text-yellow"></i>  <?php echo $this->viewBag['notification']["rmRelocation"] ?> Relocation Request
                    </a>
                </li>
                <li>
                    <a  href="<?php echo base_url() ?>existrmresponse" style="display:<?php echo ($this->viewBag['notification']["existRm"]) ? "" : "none"; ?>"id="updatenotification" notificationtype="relocationinfo">
                        <i class="fa fa-users text-yellow"></i>  <?php echo $this->viewBag['notification']["existRm"] ?> Relocation Info
                    </a>
                </li>
		<li>
                    <a  href="<?php echo base_url() ?>projectview" style="display:<?php echo ($this->viewBag['notification']["ProjectEndCount"]) ? "" : "none"; ?>"id="updatenotification" notificationtype="projectendrequest">
                        <i class="fa fa-users text-yellow"></i>  <?php echo $this->viewBag['notification']["ProjectEndCount"] ?> Project End Request
                    </a>
                </li>

                <!--                  <li>
                                    <a href="#">
                                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                    </a>
                                  </li>
                                  <li>
                                    <a href="#">
                                      <i class="fa fa-user text-red"></i> You changed your username
                                    </a>
                                  </li>-->
            </ul>
        </li>
        <!--              <li class="footer"><a href="#">View all</a></li>-->
    </ul>
</li>