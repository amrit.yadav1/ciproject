<?php
$csrf = array(
    'name' => $this->security->get_csrf_token_name(),
    'hash' => $this->security->get_csrf_hash()
);
?>
<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
<?php if (getCiInstance()->session->userdata('candidate')) { ?>
    <header class="main-header">
        <!-- Logo -->
        <a  style="background-color: #f90;padding-bottom: 55px;" href="" class="logo">
            <span class="logo-lg"><b>RECPDCL</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation" style="background-color: #f90">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                                        <!--
                    <ul class="dropdown-menu">
                        <li class="header">You have 3 messages</li>
                        <li>
                             inner menu: contains the actual data 
                            <ul class="menu">
                                <li> start message 
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="http://www.iconshock.com/img_vista/WINDOWS8/general/jpg/notepad_icon.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Billing Done
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                        </h4>
                                        <p>provisional Bills for June, 2016 generated.</p>
                                    </a>
                                </li> end message 
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="http://www.iconshock.com/img_vista/WINDOWS8/general/jpg/notepad_icon.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Payment Received  
                                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                        </h4>
                                        <p>(&#8377) 42,00,000 received from UPPCL.</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="http://www.iconshock.com/img_vista/WINDOWS8/general/jpg/notepad_icon.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Pending Bill
                                            <small><i class="fa fa-clock-o"></i> Today</small>
                                        </h4>
                                        <p>Income Tax bill (July , 2016) can be raise.</p>
                                    </a>
                                </li>
                               
                               
                            </ul>
                        </li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li>-->
                    <!-- Notifications: style can be found in dropdown.less -->
                    <!--                <li class="dropdown notifications-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-bell-o"></i>
                                            <span class="label label-warning">4</span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="header">You have 4 notifications</li>
                                            <li>
                                                 inner menu: contains the actual data 
                                                <ul class="menu">
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-users text-aqua"></i> Total 6 user activated.
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-warning text-green"></i> 325 SMS used this month.
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-warning text-yellow"></i> Profile of two Beneficiary is pending.
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-warning text-yellow"></i> Date format master setting pending.
                                                        </a>
                                                    </li>
                                                    
                                                </ul>
                                            </li>
                                            <li class="footer"><a href="#">View all</a></li>
                                        </ul>
                                    </li>-->
                    <!-- Tasks: style can be found in dropdown.less -->

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo base_url(); ?>assets/template/dist/img/logo2.png" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo GetCurrentUserName(); ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header" style="background-color:#f90">
                                <img src="<?php echo base_url(); ?>assets/template/dist/img/logo2.png" class="img-circle" alt="User Image">
                                <p>
                                    <?php echo GetCurrentUserName(); ?>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body hide">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">

                                <div class="pull-right">
                                    <a href="<?php echo base_url(); ?>signout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <!--  <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                      </li>
                    -->
                </ul>
            </div>
        </nav>
    </header>
<?php } else { ?>
    <header class="main-header">
        <!-- Logo -->
        <a  style="background-color: #f90;padding-bottom: 55px;" href="<?php echo base_url(); ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <img src="<?php echo base_url(); ?>assets/images/rec.png" alt="User Image" width="150" height="50">
            <!-- <span class="logo-mini"><b>REC</b></span> -->
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>REC</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation" style="background-color: #f90">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="">
                        <a href="<?php echo base_url() ?>knowledgecenter" title="Knowledge center" target="_blank" style="margin-top: 6px;">Knowledge center</a>
                    </li>
                    <li class="">
                        <a href="<?php echo base_url() ?>support" title="Online Ticket" target="_blank" style="margin-top: 6px;"> Online Ticket</a>
                    </li>
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="">
                        <a href="<?php echo base_url() ?>help" title="HELP" target="_blank">
                            <i class="fa fa-question-circle"></i>
                            <span class="label label-success"></span>
                        </a>
                    </li>
                    <!--
                    <ul class="dropdown-menu">
                        <li class="header">You have 3 messages</li>
                        <li>
                             inner menu: contains the actual data 
                            <ul class="menu">
                                <li> start message 
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="http://www.iconshock.com/img_vista/WINDOWS8/general/jpg/notepad_icon.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Billing Done
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                        </h4>
                                        <p>provisional Bills for June, 2016 generated.</p>
                                    </a>
                                </li> end message 
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="http://www.iconshock.com/img_vista/WINDOWS8/general/jpg/notepad_icon.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Payment Received  
                                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                        </h4>
                                        <p>(&#8377) 42,00,000 received from UPPCL.</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="http://www.iconshock.com/img_vista/WINDOWS8/general/jpg/notepad_icon.jpg" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Pending Bill
                                            <small><i class="fa fa-clock-o"></i> Today</small>
                                        </h4>
                                        <p>Income Tax bill (July , 2016) can be raise.</p>
                                    </a>
                                </li>
                               
                               
                            </ul>
                        </li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li>-->
                    <!-- Notifications: style can be found in dropdown.less -->
                    <!--                <li class="dropdown notifications-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-bell-o"></i>
                                            <span class="label label-warning">4</span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="header">You have 4 notifications</li>
                                            <li>
                                                 inner menu: contains the actual data 
                                                <ul class="menu">
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-users text-aqua"></i> Total 6 user activated.
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-warning text-green"></i> 325 SMS used this month.
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-warning text-yellow"></i> Profile of two Beneficiary is pending.
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-warning text-yellow"></i> Date format master setting pending.
                                                        </a>
                                                    </li>
                                                    
                                                </ul>
                                            </li>
                                            <li class="footer"><a href="#">View all</a></li>
                                        </ul>
                                    </li>-->
                    <!-- Tasks: style can be found in dropdown.less -->

                    <!-- User Account: style can be found in dropdown.less -->
                    <?php $this->load->view('partial/notification_view'); ?>

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo base_url(); ?>assets/template/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo GetCurrentUserName(); ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header" style="background-color:#f90">
                                <img src="<?php echo base_url(); ?>assets/template/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                <p>
                                    <?php echo GetCurrentUserName(); ?>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body hide">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">

                                <div class="pull-right">
                                    <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <!--  <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                      </li>
                    -->
                </ul>
            </div>
        </nav>
    </header>
<?php } ?>
