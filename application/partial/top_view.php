<!DOCTYPE html>
<html>
    <head>

        <!-- assets view content -->
        <?php $this->load->view('partial/new_css_view'); ?>

    </head>
    <body class="hold-transition skin-blue sidebar-mini" ng-app="RECAPP"  class="ng-cloak">

        <!--  Header view content  -->
        <?php $this->load->view('partial/header_view'); ?>
        <!--  Menu view content  -->
        <?php $this->load->view('partial/menu_view'); ?>