<!-- jQuery 2.1.4 -->
<script src="<?php echo base_url(); ?>assets/template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script><!-- anular js -->
<script src="<?php echo base_url(); ?>assets/myjs/angular.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/angular-cookies/angular-cookies.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/angular-route/angular-route.js"></script>

<script src="<?php echo base_url(); ?>assets/myjs/angular-sanitize.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/angularjs-toastr/angular-toastr.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/angularjs-toastr/angular-toastr.tpls.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url(); ?>assets/template/bootstrap/js/bootstrap.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>assets/template/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>assets/template/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>assets/template/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- Morris.js charts -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>assets/template/plugins/morris/morris.min.js"></script>-->
<!-- Sparkline -->
<!--<script src="<?php echo base_url(); ?>assets/template/plugins/sparkline/jquery.sparkline.min.js"></script>-->
<!-- jvectormap -->
<!--<script src="<?php echo base_url(); ?>assets/template/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>-->
<!--<script src="<?php echo base_url(); ?>assets/template/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>-->
<!-- jQuery Knob Chart -->
<!--<script src="<?php echo base_url(); ?>assets/template/plugins/knob/jquery.knob.js"></script>-->
<!-- daterangepicker -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>-->
<!--<script src="<?php echo base_url(); ?>assets/template/plugins/daterangepicker/daterangepicker.js"></script>-->
<!-- datepicker -->
<!--<script src="<?php echo base_url(); ?>assets/template/plugins/datepicker/bootstrap-datepicker.js"></script>-->
<!-- Bootstrap WYSIHTML5 -->
<!--<script src="<?php echo base_url(); ?>assets/template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>-->
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>assets/template/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<!--<script src="<?php echo base_url(); ?>assets/template/plugins/fastclick/fastclick.min.js"></script>-->
<!-- AdminLTE App -->
<!--<script src="<?php echo base_url(); ?>assets/template/dist/js/app.min.js"></script>-->

<!--Angular datepicker -->
<script src="<?php echo base_url(); ?>assets/template/plugins/angular-datepicker.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/myjs/common.js"></script>
<script src="<?php echo base_url(); ?>assets/myjs/angular_pagination/ui-bootstrap-tpls.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/angular-ui-select/dist/select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/projectscripts/app.js"></script>
<script src="<?php echo base_url(); ?>assets/projectscripts/constant.js"></script>
<script src="<?php echo base_url(); ?>assets/projectscripts/filter.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/angucomplete-alt/dist/angucomplete-alt.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template/datatable/angular-datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template/datatable/jquery.dataTables.min.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/bower_components/blockUI/angular-block-ui.min.js"></script>-->
<script src="<?php echo base_url(); ?>assets/bower_components/d3/d3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/ng-knob/dist/ng-knob.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/angular-file-upload/dist/angular-file-upload.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/angular-confirm/dist/angular-confirm.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-table2excel/dist/jquery.table2excel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/d3/3.5.6/d3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/nvd3/build/nv.d3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/angular-nvd3/dist/angular-nvd3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/highchart/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/sweet/SweetAlert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/sweet/SweetAlert.js"></script>


<script type="text/javascript">
    $('#updatenotification').click(function(){
//        alert($(this).attr('notificationtype'));
    })
</script>