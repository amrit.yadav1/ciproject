<!DOCTYPE html>
<html>
    <head>

        <!-- assets view content -->
        <?php $this->load->view('partial/new_css_view'); ?>

    </head>
    <body class="hold-transition skin-blue fixed sidebar-mini" ng-app="RECAPP"  ng-cloak>

        <!--  Header view content  -->
        <?php $this->load->view('partial/header_view'); ?>
        <!--  Menu view content  -->
        <?php $this->load->view('partial/menu_view'); ?>