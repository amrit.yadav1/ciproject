<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> &#946  1.1.0
    </div>
    <strong>Copyright &copy; 2017-2018 <a href="http://www.recpdcl.in/" target="_blank">REC Power Development and Consultancy Limited </a>.</strong> All rights reserved.
    <?php
    if (strstr($_SERVER["REQUEST_URI"], "invpdcl/")) {
        ?>
        <span style="color:red;font-weight:bold;font-size: 20px">This is a development Portal</span>
    <?php } else if (strstr($_SERVER["REQUEST_URI"], "recpdcl/")) { ?>
        <span style="color:red;font-weight:bold;font-size: 20px">UAT Server of RECPDCL Application.</span>
    <?php } ?>

</footer>
<!-- Control sidebar view content(removed)-->
