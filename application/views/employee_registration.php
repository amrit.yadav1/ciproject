<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Registration Page</title>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>
</head>
<script src="<?php echo base_url() ?>assets/angular.min.js"></script>
	<style type="text/css">
	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
		text-decoration: none;
	}

	a:hover {
		color: #97310e;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
		min-height: 96px;
	}

	p {
		margin: 0 0 10px;
		padding:0;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
		width: 50%;
		margin-left: 30%;
	}
	</style>
</head>
<body ng-app="app" ng-controller='subCtrl' >
<form name="form"  >
<div id="container">
<h2 class="mb-5">Registration Form</h2>

	<div id="body">
		<div class='row'>

			<div class='col-2'>
				<div class="form-group">
	<label>User Name</label></div></div>
	<div class='col-3'><div class="form-group">
<input type="text" name="uname" placeholder="User Name" ng-model="data.name"></div></div></div>

<div class='row'>
<div class='col-2'><div class="form-group"><label>Password</label></div></div>
<div class='col-3'><div class="form-group">
<input type="password" name="password" placeholder="Password" ng-model="data.password"></div></div></div>

<div class='row'>
<div class='col-2'><div class="form-group"><label>Email</label></div></div>
<div class='col-3'><div class="form-group">
<input type="text" name="email" placeholder="email" ng-model="data.email"></div></div></div>


<div class='row'>
<div class='col-2'><div class="form-group"><label>Address</label></div></div>
<div class='col-3'><div class="form-group">
<input type="text" name="Address" placeholder="address" ng-model="data.Address"></div></div></div>

<div class='row'><div class='col-2'></div>
<div class='col-3'>
<button type="submit" ng-click='submit()'>Submit</button></div>
	</div>
	
	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. </p>
</div>
</div></form>
<script src="<?php echo base_url() ?>assets/myjs/submit.js?a=<?php echo rand(0, 50) ?>"></script>
 <script>
   var BASE_URL="<?php echo base_url() ?>";
 </script>
</body>
</html>
