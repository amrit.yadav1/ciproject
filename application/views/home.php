<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>

	<title>Welcome to Home</title>

	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}



		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
			text-decoration: none;
		}

		a:hover {
			color: #97310e;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#body {
			margin: 0 15px 0 15px;
			min-height: 96px;
		}

		p {
			margin: 0 0 10px;
			padding: 0;
		}

		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>

</head>
<script src="<?php echo base_url() ?>assets/angular.min.js"></script>
<script src="https://rawgithub.com/gsklee/ngStorage/master/ngStorage.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.4/angular.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

<body>
	<form name="form" ng-controller='viewCtrl' ng-app="app">

		<div id="container">
			<div class="pull-left; margin-left: 40px;"><?php echo "Welcome:- " .$_SESSION["name"];?></div>
			<div style="text-align:right;margin-right: 20px;"><a href="<?php echo base_url() ?>logout" class="btn btn-green add-btn btn-w147">Sign out</a></div>
<div style="text-align: right;margin-right: 40px;">


<p></p>
			<div id="body">
				<table class="table table-bordered table-hover" id="table">
					<thead>
						<th>Id</th>

						<th>Name</th>
						<th>Email Id </th>
						<th>Action</th>
					</thead>
					<tbody>
						<!-- {{userList}} -->
						<tr ng-repeat="obj in userList">

							<td>{{$index+1}}</td>

							<td>{{obj.name}}</td>

							<td>{{obj.email}}</td>
							<td><a href ng-click="editdata(obj)" class="btn btn-success" data-toggle="modal" data-target="#actionModal">Edit</a>
							
							<!-- <button class='' id="export" ng-click="export()">Export to excel</button> -->
							<button class="" ng-model="exportPDF" ng-click="export(obj)">Export To PDF</button>
							<button class="" ng-click="exportExcel(obj)"><i class="fa fa-download"></i> Export as Excel</button>
						
</div>
</td>
						</tr>
					</tbody>

					
				</table>
				<div class="modal fade fade bs-example-modal-lg" tabindex="-1" id="actionModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<p><b>Update Employee Details</b></p>
							<table class="table table-bordered table-hover" id="export">
								<thead>
									<th>Name</th>
									<th>Email Id </th>
									<th>Action </th>
								</thead>
								<tbody>
									<tr>
										<td><input type='text' ng-model="values.name" /></td>
										<td><input type='text' ng-model="values.email" /></td>
										<td><button type="Update" ng-click='update(values)'>Update</button>
										</td>
									</tr>

								</tbody>
							</table>

						</div>
					</div>


				</div>

				<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
			</div>
	</form>
	<script src="<?php echo base_url() ?>assets/myjs/viewdata.js?a=<?php echo rand(0, 50) ?>"></script>
	<script>
		var BASE_URL = "<?php echo base_url() ?>";
	</script>

</body>

</html>

