<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Login Page</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>
<!-- //local storage -->
<script data-require="angular.js@1.1.5" data-semver="1.1.5" src="http://code.angularjs.org/1.1.5/angular.min.js"></script>
    <script src="https://rawgithub.com/gsklee/ngStorage/master/ngStorage.js"></script>
	
</head>
<script src="<?php echo base_url() ?>assets/angular.min.js"></script>
	<style type="text/css">
	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
		text-decoration: none;
	}

	a:hover {
		color: #97310e;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
		min-height: 96px;
	}

	p {
		margin: 0 0 10px;
		padding:0;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin-top: -42px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
		height: 88%;
	}
	</style>
</head>
<body>
<form name="form" ng-controller='myCtrl' ng-app="app" >


<div id="container" >
<section class="vh-150" style="background-color: #508bfc;">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card shadow-2-strong" style="border-radius: 1rem;">
          <div class="card-body p-5 text-center">

            <h3 class="mb-5">Sign in</h3>

            <div class="form-outline mb-4">
			<label class="form-label" for="typeEmailX-2"><b>User Name</b></label>
              <input type="uname" id="typeEmailX-2" class="form-control form-control-lg" ng-model="data.name" />
             
            </div>

            <div class="form-outline mb-4">
			<label class="form-label" for="typePasswordX-2"><b>Password</b></label>
              <input type="password" id="typePasswordX-2" class="form-control form-control-lg" ng-model="data.password" />
  
            </div>

            <!-- Checkbox -->
            <div class="form-check d-flex justify-content-start mb-4">
              <input class="form-check-input" type="checkbox" value="" id="form1Example3" />
              <label class="form-check-label" for="form1Example3"> Remember password </label>
            </div>

            <button class="btn btn-primary btn-lg btn-block" type="submit" ng-click='login()'>Login</button>

			<p class="mb-5 pb-lg-2" style="color: #393f81;">Don't have an account? <a href="<?php echo base_url(); ?>registration" style="color: #393f81;">Register here</a></p>

<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. </p>
</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>
</form>
<script src="<?php echo base_url() ?>assets/myjs/login.js?a=<?php echo rand(0, 50) ?>"></script>
 <script>
   var BASE_URL="<?php echo base_url() ?>";
 </script>
</body>
</html>
