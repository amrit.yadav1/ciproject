
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
<button id="export">Export</button>
<table id="export">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Age</th>
        <th>Address</th>
    </tr>
    <tr>
        <td>01</td>
        <td>Alpha</td>
        <td>37</td>
        <td>Bandung</td>
    </tr>
    <tr>
        <td>02</td>
        <td>Bravo</td>
        <td>29</td>
        <td>Bali</td>
    </tr>
</table>
<!-- <table id="table">
  <thead>
    <tr>
      <td class="noExport">This cell won't be exported.</td>
      <td>This cell will get exported.</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Lorem</td>
      <td class="noExport">Ipsum</td>
    </tr>
    <tr>
      <td>Foo</td>
      <td>Bar</td>
    </tr>
  </tbody>
</table> -->
<!-- <script >
    $(document).ready(function() {
    $('#export').on('click', function(e){
        $("#table").table2excel({
            exclude: ".noExport",
            name: "Data",
            filename: "Workbook",
        });
    });
});
</script> -->
<script>
 $(document).ready(function () {
    $("#export").table2excel({
        filename: "Employees.xls"
    });
 });
</script>