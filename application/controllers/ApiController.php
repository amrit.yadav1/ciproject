<?php
defined('BASEPATH') or exit('No direct script access allowed');
// error_reporting(-1);
// ini_set('display_errors', 1);
class ApiController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('ApiModel', 'obj');
        
    }
  
public function getApiData()
{
    $data = json_decode($this->input->raw_input_stream, TRUE);
    
    $token = $this->obj->checktoken($data);
    // print_r($token); exit;
    if($token)
    {
        $UserData =  $this->obj->getApiData($data);
        if (!empty($UserData)) {
            echo new_method($UserData, 'Data Load Successfully', 'true');
        } else {
            echo new_method(-1, 'No Data Found', 'false');
        }
        exit;
    }
    else
    {
        echo new_method(-1, 'You are not a valid user', 'false');
    }
    
}
public function invokeapidata()
{

    echo "<pre>";
    // $url="https://new.aol.com/productsweb/subflows/ScreenNameFlow/AjaxSNAction.do?s=username&f=firstname&l=lastname";
    // $url = "http://localhost/CodeIgniter/getApiData";
    $url = "https://jsonplaceholder.typicode.com/posts";
    $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt($ch, CURLOPT_URL, $url);
    $result = curl_exec($ch);
    $data = json_decode($result, true);
    //  pre($data); exit;
$dataval = array(); // create new array

    foreach ($data as $rawdata) {
        
        // print_r($rawdata);
        // exit;
        // unset($rawdata['id']); //using unset for remove id in new array

        $dataval[] = $rawdata; // assign value in new array

    }

    // pre($dataval); exit;


    $this->db->insert_batch('insertbulk_data', $dataval);

    $insert_id = $this->db->insert_id();

    if ($insert_id > 0) {
        echo new_method($dataval, 'Update Data Successfully', 'true');
    } else {
        echo new_method(-1, 'data is not update', 'false');
    }
    
}
public function accesswithparameter()
{
    $data= [
        
        // 'token_key' => 'Vdj9beFtrX9iLABp3shYz2jeLMaWKsUrH45nuFkY3FhMngxfrc'
        'token_key' => $this->session->userdata('token_key')

    ]; 
    // print_r($data);exit;
   
    $this->curlPost('http://localhost/CodeIgniter/getApiData', $data);
    
   
}

 public function curlPost($url, $data) {
    // echo "<pre>";
      $ch = curl_init($url);
    
   
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt(
        $ch, 
        CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json', // if the content type is json
            // 'token_key: '.$token // if you need token in header
        )
    );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       $response = curl_exec($ch);

    $error = curl_error($ch);
    curl_close($ch);
    if ($error !== '') {
        throw new \Exception($error);
    }

    // return $response;
    $UserData =  $this->obj->insertApiData($response);
    if (!empty($UserData)) {
        echo new_method($UserData, 'Data insert Successfully', 'true');
    } else {
        echo new_method(-1, 'Not inserted Data ', 'false');
    }
    exit;
}
}