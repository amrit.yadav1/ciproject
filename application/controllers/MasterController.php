<?php
defined('BASEPATH') or exit('No direct script access allowed');
// error_reporting(-1);
// ini_set('display_errors', 1);
class MasterController extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library("excel");
        $this->load->model('MasterModel', 'obj');
       
        
    }
  

    public function home()
    {

        $this->load->view('home.php');
    }

    public function getUserData()
    {
// pre("APPPATH"); exit;

        $data = json_decode($this->input->raw_input_stream, TRUE);

        $checktokenval = $this->obj->checkacesstoken($data); //get acess token from

        if ($checktokenval) {

            $UserData =  $this->obj->getUserData($data);
            // print_r($UserData);exit();
            if (!empty($UserData)) {

                echo new_method($UserData, 'Data Load Successfully', 'true');

            } else {
                echo new_method(-1, 'No Data Found', 'false');
            }
            exit;
        } else {
            echo new_method(-1, 'You are not valid user', 'false');
        }
    }
    public function updateData()
    {
        $data = json_decode($this->input->raw_input_stream, TRUE);
        // print_r($data);exit();
        $UserData = $this->obj->updateData($data);
        if (!empty($UserData)) {
            echo new_method($UserData, 'Update Data Successfully', 'true');
        } else {
            echo new_method(-1, 'data is not update', 'false');
        }
        exit;
    }

    public function logout()
    {

        $this->session->sess_destroy();

        redirect(base_url() . "login", 'refresh');
    }
    public function getAllEmployeeExcel() {
        // echo REPORT_EXCEL_PATH; exit;
        $reqParameter = array(
            'id' => $this->input->get('id'),
            'name' => $this->input->get('name'),
            'email' => $this->input->get('email'),
            
                 );
                //  print_r($reqParameter);exit();

        $objPHPExcel = new PHPExcel();
            $filename = 'emp_detail' . time() . '.xlsx';
            $objWorkSheet = $objPHPExcel->createSheet(0);
            $objWorkSheet->setTitle('Application or Operating');
            $objPHPExcel->setActiveSheetIndex(0);
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );
            $cellvalstyle = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                )
            );
            // $header1Array = ['S.No', 'Client Name', 'Period', 'STU Charges', 'Wheeling Charges', 'Total'];
            $header1Array = ['S.No', 'Client Name', 'Email'];
            // $objPHPExcel->getActiveSheet()->fromArray($header1Array, NULL, 'A1', true)->getStyle("A1:F1")->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->fromArray($header1Array, NULL, 'A1', true)->getStyle("A1:C1")->applyFromArray($style);
            
            $strtRow = 3;
            $i = 1;
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $strtRow, $reqParameter['id'])->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $strtRow, $reqParameter['name'])->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $strtRow, $reqParameter['email'])->getColumnDimension('C')->setWidth(20);
            // foreach ($reqParameter  as $key => $value) {
            //     $objPHPExcel->getActiveSheet()->setCellValue('A' . $strtRow, $i);
            //     $objPHPExcel->getActiveSheet()->setCellValue('B' . $strtRow, $value['client_name'])->getColumnDimension('B')->setWidth(50);
            //     $objPHPExcel->getActiveSheet()->setCellValue('C' . $strtRow, $value['client_name'])->getColumnDimension('B')->setWidth(50);
            //     // $objPHPExcel->getActiveSheet()->setCellValue('C' . $strtRow, date('d-m-Y', strtotime($value['week_start_date'])) . ' To ' . date('d-m-Y', strtotime($value['week_end_date'])))->getColumnDimension('C')->setWidth(30);
            //     // $objPHPExcel->getActiveSheet()->setCellValue('D' . $strtRow, $value['stu_charge'])->getColumnDimension('D')->setWidth(15);
            //     // $objPHPExcel->getActiveSheet()->setCellValue('E' . $strtRow, $value['wheeling_charge'])->getColumnDimension('E')->setWidth(20);
            //     // $objPHPExcel->getActiveSheet()->setCellValue('F' . $strtRow, $value['wheeling_charge'] + $value['stu_charge'])->getColumnDimension('F')->setWidth(15);
            //     $strtRow++;
            //     $i++;
            // }
            $tempDir = APPPATH . '../' . REPORT_EXCEL_PATH;
            $downloadpath = REPORT_EXCEL_PATH;
            if (is_dir($tempDir)) {
                mkdir($tempDir, 0777, true);
            }
            $filePath = $tempDir . '/' . $filename;
            $filedownload = $downloadpath . '/' . $filename;
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save($filePath);
            
          
        $fnp = $filePath;
        $fnt = "application/xls";
        $fnn = $filename;
        header('Cache-Control: max-age=31536000');

        header('Expires: Mon, 25 Sep 2017 05:00:00 GMT');

        header('Content-Length: ' . filesize($fnp));

        header('Content-Disposition: filename="' . $fnn . '"');

        header('Content-Type: ' . $fnt . '; name="' . $fnn . '"');

        readfile($fnp);
        unlink($fnp);

    }

// convert pdf

  
}
