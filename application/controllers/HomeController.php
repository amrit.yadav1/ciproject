<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(-1);
ini_set('display_errors', 1);
class HomeController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('HomeModel', 'obj');
        // $this->load->helper('custom'); //custom_helper.php
    }

    public function login()
    {
        $this->load->view('login.php');
    }
    public function registration()
    {
        $this->load->view('employee_registration.php');
    }
    public function downloadexcel()
    {
        $this->load->view('exporttoexcel.php');
    }
    public function submit()
    {
        $data = json_decode($this->input->raw_input_stream, TRUE);

        $query = $this->obj->submitdetails($data);

        if (!empty($query)) {


            echo new_method($query, 'registration is successfully', 'true');

        } else {
            echo new_method(-1, 'registration is not matched', 'false');
        }
        exit;
    }
    public function logindetails()
    {
        $data = json_decode($this->input->raw_input_stream, TRUE);

        $query = $this->obj->logindetails($data);

        if (!empty($query)) {


            echo new_method($query, 'login successfully', 'true');
            
            
        } else {
            echo new_method(-1, 'login details not matched', 'false');
        }
        exit;
    }
   
}
