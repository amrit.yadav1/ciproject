<?php

class Excelconvert {

    public function Get_Excel($insertitle, $sheetdata, $name, $rowHeading = 0) {


        $date = date('d-m-Y');
        $rowShift = 0;

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("RECPDCL")
                ->setLastModifiedBy("RECPDCL")
                ->setTitle("REPORT")
                ->setSubject("REPORT")
                ->setDescription("REPORT");
        $objPHPExcel->setActiveSheetIndex(0);
//        $date = 0000;

        $day_count = 1;


//Alignment
        $objPHPExcel->getActiveSheet()->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //Alignment


        $objWorkSheet = $objPHPExcel->createSheet($day_count);
        $objWorkSheet->setTitle($date);
        $objPHPExcel->setActiveSheetIndex($day_count);

        $rowCount = 2;
        $objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("AA1")->getFont()->setBold(true);
        
        for ($col = 'A'; $col !== 'Z'; $col++) {
            
            
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setWidth(25);
        }

        if ($rowHeading > 0) {
            foreach ($rowHeading as $key => $val) {

                $rowShift = $key + 1;

                $objPHPExcel->getActiveSheet()->mergeCells("A$rowShift:O$rowShift");
                $objPHPExcel->getActiveSheet()->getStyle("A$rowShift:Z$rowShift")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getCell("A" . $rowShift)->setValue($val);
            }
            $titlelocation = $rowShift + 1;
            $objPHPExcel->getActiveSheet()->getStyle("A$titlelocation:Z$titlelocation")->getFont()->setBold(true);
        }

        $row = 1 + $rowShift;
        $col = 0;
        foreach ($insertitle as $key1 => $val) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $val);
            $col++;
        }
        $row = 2 + $rowShift;
        foreach ($sheetdata as $key => $value) {
            $col = 0;
            foreach ($value as $key1 => $val) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $val);
                $col++;
            }
            $row++;
            $rowCount++;
        }

        $filename = "Report" . date('d.m.Y') . '.xls';
        ob_get_clean();
        ob_start();
        $tempDir = "/tmp/CodeIgniter/files/report/tmpStore" . rand(1, 99);
        if (!is_dir($tempDir)) {
            mkdir($tempDir, 0777, true);
        }
        $filePath = $tempDir . "/" . "Report" . date('d.m.Y') . '.xls';

        //  to save in directory
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($filePath);


        $fnp = $filePath;
        $fnt = "application/xls";
        $fnn = $name . " " . date('d.m.Y') . '.xls';
        header('Cache-Control: max-age=31536000');

        header('Expires: Mon, 25 Sep 2017 05:00:00 GMT');

        header('Content-Length: ' . filesize($fnp));

        header('Content-Disposition: filename="' . $fnn . '"');

        header('Content-Type: ' . $fnt . '; name="' . $fnn . '"');

        readfile($fnp);
        unlink($fnp);
        //Excel convert Code end 
    }

}
