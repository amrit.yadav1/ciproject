var app = angular.module('app', ['ngStorage']);   // angular module - define
app.controller('viewCtrl', function ($scope, $http,$window,$localStorage) {
    
console.log($localStorage);
   $scope.getUserData = function () {
    var obj={
        access_token:$localStorage.loginuser.token_key
    }
        //  alert('hi');
        $http({
            method: "POST",
            url: BASE_URL + 'getUserData',
            data: JSON.stringify(obj),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function mySuccess(response) {
            $scope.userList = response.data.value;
      
            console.log(response.data);
        }, function myError(response) {

        });
        
    }
    //use for view the data in modal
    // $scope.getUserData();
    $scope.editdata = function (obj) {
        $scope.values=obj;

    }
    $scope.getUserData();
    

    $scope.update = function (values) {
        $http({
            method: "POST",
            url: BASE_URL + 'updateData',
            data: JSON.stringify($scope.values),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }

        }).then(function mySuccess(response){
            $scope.update = response.data.value;
            console.log(response.data);

            if(response.data.type=="true")
            {
                alert(response.data.message);
                $('#actionModal').modal("hide");
            }
            else{
                alert(response.data.message);

            }
        
        })
       
    }
   
    $scope.export =function() {
        // alert('hi'); 
        $("#table").table2excel({
            exclude: ".noExport",
            name: "Data",
            filename: "Workbook",
        });
        
    }
    $scope.exportExcel = function (obj) {
        //alert('hi'); 
        
      var postdata = {
          'id':obj.id,
          'name':obj.name,
          'email':obj.email
          
      };
    //   console.log(postdata);
      window.open(BASE_URL+'getAllEmployeeExcel?id=' + obj.id + '&name=' + obj.name + '&email=' + obj.email );
      
  };
   //convert for PDF

     
   $scope.export = function(obj){
    // alert('hi.....');
    $scope.data=[{'id':obj.id,'name':obj.name,'email':obj.email}];
    // console.log($scope.data);
      html2canvas(document.getElementById('table'), {
          onrendered: function (canvas) {
              var data = canvas.toDataURL();
              var docDefinition = {
                  content: [{
                      image: data,
                      width: 500,
                  }]
              };
              console.log(docDefinition);
              pdfMake.createPdf(docDefinition).download("test.pdf");
          }
      });
   }
 

});